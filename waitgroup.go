package go_defer_reserach

import (
	"sync"
	"sync/atomic"
	"time"
)

func WaitGroupSuccess(n, after int) (complete bool, recovered uint32) {
	complete = false
	recovered = 0

	var wg = new(sync.WaitGroup)

	for i := 1; i <= n; i++ {
		wg.Add(1)

		go func(i int) {
			defer func() {
				if err := recover(); err != nil {
					atomic.AddUint32(&recovered, 1)
				}
			}()

			defer wg.Done()

			panicAfterN(i, after)
		}(i)
	}

	wg.Wait()

	complete = true

	return
}

func panicAfterN(i, after int) {
	time.Sleep(time.Millisecond)

	if i%after == 0 {
		panic("i%after == 0")
	}
}
