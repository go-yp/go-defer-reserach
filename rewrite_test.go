package go_defer_reserach

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestReturnOne(t *testing.T) {
	assert.Equal(t, 1, ReturnOne())
}

func TestRewriteReturnOne(t *testing.T) {
	assert.Equal(t, 2, RewriteReturnOne())
}

func TestRewriteReturnOneWithoutAssign(t *testing.T) {
	assert.Equal(t, 3, RewriteReturnOneWithoutAssign())
}

func TestModifyReturnOneWithoutAssign(t *testing.T) {
	assert.Equal(t, 10, ModifyReturnOneWithoutAssign())
}
