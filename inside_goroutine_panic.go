package go_defer_reserach

import "sync"

type PanicFunctionState struct {
	Completed bool
}

func InsideGorountinePanic(state *PanicFunctionState, n, after int) {
	var wg = new(sync.WaitGroup)

	for i := 1; i <= n; i++ {
		wg.Add(1)

		go func(i int) {
			// not work "defer recover()"
			// work, before and after wg.Done()
			/**
			defer func() {
				recover()
			}()
			*/

			defer wg.Done()

			panicAfterN(i, after)
		}(i)
	}

	wg.Wait()

	state.Completed = true

	return
}
