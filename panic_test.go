package go_defer_reserach

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPanic(t *testing.T) {
	{
		var actual, recovered = Panic(false)

		assert.Equal(
			t,
			[]string{"first append", "first defer append"},
			actual,
		)
		assert.Equal(t, false, recovered)
	}

	{
		var actual, recovered = Panic(true)

		assert.Equal(
			t,
			[]string{"first append", "test panic", "first defer append"},
			actual,
		)
		assert.Equal(t, true, recovered)
	}
}

func TestWrapRecovery(t *testing.T) {
	var state = new(State)

	WrapRecovery(state)

	assert.Equal(
		t,
		[]string{
			"first",
			"second",
			"third",
			"third defer — without recover",
			"second defer — recover string panic: catch me",
			"first defer — without panic",
		},
		state.Values(),
	)
}

func TestNestedPanic(t *testing.T) {
	var state = new(State)

	NestedPanic(state)

	assert.Equal(
		t,
		[]string{
			"0 level",
			"1 level",
			"2 level",
			"2 level — defer",
			"1 level — defer",
			"0 level — recover string panic: 2 level — panic",
		},
		state.Values(),
	)
}

func TestPanicInsideRecover(t *testing.T) {
	var state = new(State)

	PanicInsideRecover(state)

	assert.Equal(
		t,
		[]string{
			"first",
			"second",
			"second defer — recover string panic: catch me",
			"first defer — recover string panic: inside defer",
		},
		state.Values(),
	)
}
