package go_defer_reserach

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestOneDeferOrder(t *testing.T) {
	var actual = OneDeferOrder()

	assert.Equal(
		t,
		actual,
		[]string{
			"first",
			"second",

			"first defer",
		},
	)
}

func TestDoubleDeferOrder(t *testing.T) {
	var order = DoubleDeferOrder()

	assert.Equal(
		t,
		order,
		[]string{
			"first",
			"second",
			"third",

			"second defer",
			"first defer",
		},
	)
}

func TestOnlyLastHandleDefer(t *testing.T) {
	var state = new(State)

	OnlyLastHandleDefer(state)

	assert.Equal(
		t,
		state.Values(),
		[]string{
			"first",
			"first defer — first call",
			"first defer — second call",
			"second",
			"first defer — last call",
		},
	)
}

func TestOnlyLastHandleDeferWrap(t *testing.T) {
	var state = new(State)

	OnlyLastHandleDeferWrap(state)

	assert.Equal(
		t,
		state.Values(),
		[]string{
			"first",
			"second",
			"first defer — first call",
			"first defer — second call",
			"first defer — last call",
		},
	)
}
