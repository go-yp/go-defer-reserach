package go_defer_reserach

type State struct {
	values []string
}

func (s *State) Append(value string) *State {
	s.values = append(s.values, value)

	return s
}

func (s *State) Values() []string {
	return s.values
}
