package go_defer_reserach

func CallInside(state *State) {
	var counter = new(StringCounter)

	state.Append("first call " + counter.Next())

	defer state.Append("first defer call " + counter.Next())

	state.Append("second call " + counter.Next())
}

func CallInsideWrap(state *State) {
	var counter = new(StringCounter)

	state.Append("first call " + counter.Next())

	defer func() {
		state.Append("first defer call " + counter.Next())
	}()

	state.Append("second call " + counter.Next())
}
