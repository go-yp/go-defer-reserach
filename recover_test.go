package go_defer_reserach

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRecover(t *testing.T) {
	var expect interface{}

	var actual = recover()

	assert.Equal(t, true, expect == actual)
}

// just for fun
// BenchmarkRecover   	2.02 ns/op	       0 B/op	       0 allocs/op
func BenchmarkRecover(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = recover()
	}
}
