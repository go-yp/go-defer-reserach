package go_defer_reserach

func OneDeferOrder() (result []string) {
	result = append(result, "first")
	defer func() {
		result = append(result, "first defer")
	}()

	result = append(result, "second")

	return result
}

func DoubleDeferOrder() (result []string) {
	result = append(result, "first")
	defer func() {
		result = append(result, "first defer")
	}()

	result = append(result, "second")
	defer func() {
		result = append(result, "second defer")
	}()

	result = append(result, "third")

	return result
}

func OnlyLastHandleDefer(state *State) {
	state.Append("first")

	defer state.
		Append("first defer — first call").
		Append("first defer — second call").
		Append("first defer — last call")

	state.Append("second")
}

func OnlyLastHandleDeferWrap(state *State) {
	state.Append("first")

	defer func() {
		state.
			Append("first defer — first call").
			Append("first defer — second call").
			Append("first defer — last call")
	}()

	state.Append("second")
}
