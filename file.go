package go_defer_reserach

import (
	"os"
)

// https://gobyexample.com/defer
func FileOperationsExample() error {
	f, err := os.Create("/tmp/defer.txt")
	if err != nil {
		return err
	}
	defer f.Close()

	// operations with file

	return nil
}
