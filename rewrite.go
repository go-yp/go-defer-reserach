package go_defer_reserach

func ReturnOne() (result int) {
	result = 1

	return
}

func RewriteReturnOne() (result int) {
	defer func() {
		result = 2
	}()

	result = 1

	return
}

func RewriteReturnOneWithoutAssign() (result int) {
	defer func() {
		result = 3
	}()

	return 1
}

func ModifyReturnOneWithoutAssign() (result int) {
	defer func() {
		result = result * 5
	}()

	return 2
}
