# Go defer reserach


# Resources
 - [Defer, Panic, and Recover](https://blog.golang.org/defer-panic-and-recover)
 - [Go by Example: Defer](https://gobyexample.com/defer)
 - [Recover only works when called from the same goroutine as the panic is called in](https://stackoverflow.com/questions/50409011/handling-panics-in-go-routines)