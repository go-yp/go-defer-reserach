package main

import (
	"fmt"
	go_defer_reserach "gitlab.com/go-yp/go-defer-reserach"
)

// did receive
// 1:
/*
panic state `false` before
panic state `true` after
panic: i%after == 0
*/
// 2:
/*
panic state `false` before
panic: i%after == 0
*/

// after `go build ./example/inside_goroutine_panic/inside_goroutine_panic.go` same chance

func main() {
	var state = new(go_defer_reserach.PanicFunctionState)

	// will not print :(
	defer func() {
		fmt.Printf("panic state `%t` after\n", state.Completed)
	}()

	fmt.Printf("panic state `%t` before\n", state.Completed)

	// will panic
	go_defer_reserach.InsideGorountinePanic(state, 25, 20)
}
