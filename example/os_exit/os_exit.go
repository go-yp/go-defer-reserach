package main

import (
	"fmt"
	"os"
)

// os.Exit ignore defer, output will "first call"
func main() {
	fmt.Println("first call")

	defer fmt.Println("first defer call")

	os.Exit(0)
}
