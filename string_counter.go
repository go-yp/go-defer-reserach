package go_defer_reserach

import (
	"strconv"
)

type StringCounter struct {
	value uint64
}

func (c *StringCounter) Next() string {
	c.value += 1

	var next = c.value

	return strconv.FormatUint(next, 10)
}
