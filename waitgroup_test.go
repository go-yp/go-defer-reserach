package go_defer_reserach

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestWaitGroupSuccess(t *testing.T) {
	{
		var complete, recovered = WaitGroupSuccess(5, 10)

		assert.Equal(t, true, complete)
		assert.Equal(t, uint32(0), recovered)
	}

	{
		var complete, recovered = WaitGroupSuccess(5, 3)

		assert.Equal(t, true, complete)
		assert.Equal(t, uint32(1), recovered)
	}

	{
		var complete, recovered = WaitGroupSuccess(25, 5)

		assert.Equal(t, true, complete)
		assert.Equal(t, uint32(5), recovered)
	}
}
