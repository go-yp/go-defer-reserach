package go_defer_reserach

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCallInside(t *testing.T) {
	var state = new(State)

	CallInside(state)

	assert.Equal(
		t,
		[]string{
			"first call 1",
			"second call 3",
			"first defer call 2",
		},
		state.Values(),
	)
}

func TestCallInsideWrap(t *testing.T) {
	var state = new(State)

	CallInsideWrap(state)

	assert.Equal(
		t,
		[]string{
			"first call 1",
			"second call 2",
			"first defer call 3",
		},
		state.Values(),
	)
}
