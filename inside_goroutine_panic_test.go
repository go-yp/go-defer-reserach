package go_defer_reserach

import (
	"fmt"
	"testing"
)

func TestInsideGorountinePanic(t *testing.T) {
	if testing.Short() {
		return
	}

	var state = new(PanicFunctionState)

	// will not print :(
	defer func() {
		fmt.Printf("panic state `%t` after\n", state.Completed)
	}()

	fmt.Printf("panic state `%t` before\n", state.Completed)

	// will panic
	InsideGorountinePanic(state, 25, 20)

	/**
		Result:
	panic state false before
	panic: i%after == 0
	*/
}
